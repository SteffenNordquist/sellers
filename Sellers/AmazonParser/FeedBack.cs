﻿using DN_Classes.Entities.Seller;
using HtmlAgilityPack;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Sellers
{
    public class FeedBack
    {
        WebClient webClient = new WebClient();
        HtmlDocument htmlDocument = new HtmlDocument();
        private SellerEntity sellerEntity;

        public FeedBack(SellerEntity sellerEntity)
        {
            this.sellerEntity = sellerEntity;
        }

        public DN_Classes.Entities.Seller.FeedBack Get()
        {
            try
            {
                string url = string.Format("https://www.amazon.de/gp/aag/main/ref=olp_merch_rating_8?ie=UTF8&isAmazonFulfilled=0&seller={0}", sellerEntity.details.sellerID /*"A2GIISLO3I77TP"*/);

                //String html;
                //WebRequest objRequest = WebRequest.Create(url);
                //WebResponse objResponse = objRequest.GetResponse();
                //using (var sr = new StreamReader(objResponse.GetResponseStream(), Encoding.Default))
                //{
                //    Thread.Sleep(1000);
                //    html = sr.ReadToEnd();
                //    sr.Close();
                //}

                Thread.Sleep(1000);
                webClient.Encoding = Encoding.Default;
                webClient.Headers.Add("user-agent", "Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.2; .NET CLR 1.0.3705;)");
                string html = webClient.DownloadString(url);
                htmlDocument.LoadHtml(html);

                ExtractFeedbackGTLast30();
            }

            catch (Exception ex) { Console.WriteLine(ex.Message); };

            return sellerEntity.details.feedbackHistory;
        }

        private void ExtractFeedbackGTLast30()
        {
            bool positiveLT95 = false;
            bool numberGT40 = false;
            bool hasValue = false;
            int whileCounter = 0;

            while (whileCounter <= 3)
            {
                try
                {
                    int nodeCounter = 1;
                    foreach (HtmlNode node in htmlDocument.DocumentNode.SelectNodes(".//td[@class='feedback30Days']"))
                    {
                        try
                        {
                            float value = Convert.ToSingle(node.InnerText.Replace("%", "").Replace(".", ",").Trim());
                            if (nodeCounter == 1)
                            {
                                if (value < 95)
                                    positiveLT95 = true;
                                //sellerEntity.details.feedbackHistory.positive = value;

                                sellerEntity.details.feedbackHistory.positive = value;
                                //break;                           
                            }

                            else if (nodeCounter == 4)
                            {
                                if (value > 40)
                                    numberGT40 = true;
                                //sellerEntity.details.feedbackHistory.number = Convert.ToInt32(node.InnerText.Trim());

                                sellerEntity.details.feedbackHistory.number = Convert.ToInt32(node.InnerText.Trim());
                                //break; 
                            }

                            else if (nodeCounter == 2)
                                sellerEntity.details.feedbackHistory.neutral = value;

                            else if (nodeCounter == 3)
                                sellerEntity.details.feedbackHistory.negative = value;

                            if (value > 0)
                                hasValue = true;
                        }

                        catch (Exception ex)
                        { Console.WriteLine(ex.Message); }

                        nodeCounter++;
                    }

                    if (hasValue)
                        Console.Write("\tFeedback:true");

                    else
                        Console.Write("\tFeedback:false");


                    ExtractNegativeRatings(positiveLT95, numberGT40);

                    break;
                }

                catch
                { /*no within last 30 days feedback*/ whileCounter++; }
            }
        }

        private void ExtractNegativeRatings(bool positiveGT95, bool numberGT40)
        {
            if (positiveGT95 && numberGT40)
            {
                Rating rating = new Rating(sellerEntity.details.sellerID);
                sellerEntity.details.feedbackHistory.negativeRatings = rating.GetNegativeRating();
                Console.Write("\tRating:true");
            }

            else
                Console.Write("\tRating:false");
        }
    }
}
