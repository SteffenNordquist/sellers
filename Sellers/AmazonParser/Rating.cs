﻿using HtmlAgilityPack;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;

namespace Sellers
{
    public class Rating
    {
        private WebClient webClient;
        private HtmlDocument htmlDocument;
        private readonly string sellerID = "";
        //private readonly string datePattern = @"(([0-9])|([0-2][0-9])|([3][0-1])).\-(Januar|Februar|März|April|Mai|Juni|Juli|August|September|Oktober|November|Dezember)\-\d{4}";
        private readonly string datePattern = @"\d{1,2}. (\w+) \d{4}";

        public Rating(string sellerID)
        { 
            this.webClient = new WebClient();
            webClient.Encoding = Encoding.Default;
            webClient.Headers.Add("user-agent", "Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.2; .NET CLR 1.0.3705;)");
            this.htmlDocument  = new HtmlDocument();
            this.sellerID = sellerID;
        }

        public string GetNegativeRating()
        {
            int pageIncrementor = 0;
            int badRateCounter = 0;
            int commentCounter = 0;
            int whileCounter = 0;

            while (whileCounter <= 3)
            {
                try
                {
                    string url = string.Format("http://www.amazon.de/gp/aag/ajax/paginatedFeedback.html?seller={0}&isAmazonFulfilled=0&isCBA=&marketplaceID=A1PA6795UKMFR9&ref_=aag_m_fb&&currentPage={1}", sellerID/*"A2GIISLO3I77TP"*/, pageIncrementor);

                    //String html;
                    //WebRequest objRequest = WebRequest.Create(url);
                    //WebResponse objResponse = objRequest.GetResponse();
                    //using (var sr = new StreamReader(objResponse.GetResponseStream(), Encoding.Default))
                    //{
                    //    Thread.Sleep(1000);
                    //    html = sr.ReadToEnd();
                    //    sr.Close();
                    //}

                    Thread.Sleep(1000);
                    webClient.Encoding = Encoding.Default;
                    webClient.Headers.Add("user-agent", "Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.2; .NET CLR 1.0.3705;)");
                    htmlDocument.LoadHtml(webClient.DownloadString(url));
                    // htmlDocument.LoadHtml(html);

                    foreach (HtmlNode node in htmlDocument.DocumentNode.SelectNodes(".//li/ul"))
                    {
                        try
                        {
                            DateTime usDateTime = DateTime.Parse(Regex.Match(node.SelectSingleNode(".//li[@class='feedback-rater-date']").InnerText, datePattern).Value, new CultureInfo("de-DE"));

                            if (usDateTime < DateTime.Now.AddDays(-10))
                            { whileCounter = 4;  break; }

                            else
                            {
                                try
                                {
                                    string badRate = node.SelectSingleNode(".//li[@class='feedback-num feedbackRed']").InnerText.Replace(":", "").Trim();
                                    if (badRate == "1/5" || badRate == "2/5")
                                        badRateCounter++;
                                }
                                catch
                                { /*maybe no rate or rate is Green(5&4) or Neutral(3)*/}
                            }
                        }

                        catch { /*maybe no date.*/}

                        commentCounter++;
                    }

                    pageIncrementor++;
                }

                catch
                { whileCounter++; }
            }


            return badRateCounter.ToString() + "/" +commentCounter.ToString();
        }
    }
}
