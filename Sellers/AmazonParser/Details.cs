﻿using DN_Classes.Entities.Seller;
using HtmlAgilityPack;
using MongoDB.Bson;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;

namespace Sellers
{
    public class Details
    {
        private WebClient webClient = new WebClient();
        private HtmlDocument htmlDocument = new HtmlDocument();
        private SellerEntity sellerEntity;
        private string sellerID;
        const string MatchEmailPattern =
                                            @"(([\w-]+\.)+[\w-]+|([a-zA-Z]{1}|[\w-]{2,}))@"
                                            + @"((([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\.([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\."
                                            + @"([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\.([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])){1}|"
                                            + @"([a-zA-Z]+[\w-]+\.)+[a-zA-Z]{2,4})";


        public Details(SellerEntity sellerEntity)
        {
            this.sellerEntity = sellerEntity;
        }

        public Details(string sellerID)
        {
            this.sellerID = sellerID;
        }

        public DN_Classes.Entities.Seller.Details Get()
        {
            try
            {
                string amazonLink = string.Format("http://www.amazon.de/gp/aag/details/ref=aag_m_fb?ie=UTF8&isAmazonFulfilled=0&isCBA=&marketplaceID=A1PA6795UKMFR9&seller={0}", sellerEntity.details.sellerID /*"A2LW8X3VT5SWBN"*/);
                //string amazonLink = string.Format("https://www.amazon.de/gp/aag/main/ref=olp_merch_rating_8?ie=UTF8&isAmazonFulfilled=0&seller={0}", sellerEntity.details.sellerID);

                //String html;
                //WebRequest objRequest = WebRequest.Create(amazonLink);
                //WebResponse objResponse = objRequest.GetResponse();
                //using (var sr = new StreamReader(objResponse.GetResponseStream(), Encoding.Default))
                //{
                //    Thread.Sleep(1000);
                //    strResult = sr.ReadToEnd();
                //    sr.Close();
                //}

                Thread.Sleep(1000);
                webClient.Encoding = Encoding.Default;
                webClient.Headers.Add("user-agent", "Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.2; .NET CLR 1.0.3705;)");
                string html = webClient.DownloadString(amazonLink);
                htmlDocument.LoadHtml(html);

                ExtractDetails(amazonLink, html);
            }

            catch(Exception ex)
            { /*no seller info*/ Console.WriteLine(ex.Message); return null; }

            return sellerEntity.details;
        }

        private void ExtractDetails(string amazonLink, string html)
        {
            sellerEntity.details.amazonLink = amazonLink;
            sellerEntity.details.sellerID = sellerEntity.details.sellerID;

            foreach (HtmlNode node in htmlDocument.DocumentNode.SelectNodes(".//ul/li[@class='aagLegalRow']"))
            {
                string value = node.InnerText.Split(':')[1];
                string header = node.InnerText.Split(':')[0];

                if (header.ToLower().Contains("geschäftsname"))
                    sellerEntity.details.businessName = value;

                else if (header.ToLower().Contains("geschäftsart"))
                    sellerEntity.details.typeOfBusiness = value;

                else if (header.ToLower().Contains("handelsregisternummer"))
                    sellerEntity.details.tradeRegisterNumber = value;

                else if (header.ToLower().Contains("ust"))
                    sellerEntity.details.ustID = value;

                else if (header.ToLower().Contains("unternehmensvertreter"))
                    sellerEntity.details.companyRepresentatives = value;

                else if (header.ToLower().Contains("telefonnummer"))
                    sellerEntity.details.phone = value;

                else if (header.ToLower().Contains("kundendienstadresse"))
                    sellerEntity.details.customerServiceAddress = value;

                else if (header.ToLower().Contains("geschäftsadresse"))
                    sellerEntity.details.businessAddress = value;
            }

            sellerEntity.details.email = ExtractEmail(html);
        }
       
        private static BsonArray ExtractEmail(string html)
        {
            List<string> emails = new List<string>();

            try
            {
                foreach (Match match in Regex.Matches(html, MatchEmailPattern, RegexOptions.Compiled | RegexOptions.IgnoreCase))
                {
                    emails.Add(match.Value.ToString());
                }

                BsonArray emailBsonArray = new BsonArray();

                emails.Distinct().ToList().ForEach(email =>
                {
                    emailBsonArray.Add(email);
                });

                return emailBsonArray;
            }

            catch { return null; }
        }
    }
}
