﻿using DN_Classes.Entities.Seller;
using HtmlAgilityPack;
using MongoDB.Bson;
using MongoDB.Driver.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;

namespace Sellers
{
    public class AmazonParser
    {
        private static WebClient webClient;
        private static HtmlDocument htmlDocument;
        const string MatchEmailPattern =
                                @"(([\w-]+\.)+[\w-]+|([a-zA-Z]{1}|[\w-]{2,}))@"
                                + @"((([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\.([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\."
                                + @"([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\.([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])){1}|"
                                + @"([a-zA-Z]+[\w-]+\.)+[a-zA-Z]{2,4})";
        private List<string> sellerIDs;
        private string sellerID;

        public AmazonParser(List<string> sellerIDs)
        {
            this.sellerIDs = sellerIDs;
        }

        /// <summary>
        /// Will get each sellerID's amazon information.
        /// </summary>
        /// <returns>List<SellerEntities></returns>
        public void GetSellerInfo()
        {
            #region
            //Test
            //FlexibleItems.GetSellerIDs().ForEach(sellerID =>
            //{
            //    Details detail = new Details(sellerEntity).Get();
            //    FeedBack feedback;
            //    Rating rating;

            //    if (detail != null)
            //    {
            //        Console.Write("\tDetails:true");
            //        feedback = new FeedBack(sellerEntity).Get();

            //        if (true)//statisfies the condition
            //        {
            //            rating = new Rating();
            //        }

            //        sellerEntity.plus = new BsonDocument();
            //        sellerEntity.plus["lastTimeChecked"] = DateTime.Now.Ticks;

            //        //sellerDB.Save(sellerEntity);
            //    }

            //    IAmazonSeller amazonSeller = new AmazonSeller() { Details = detail, FeedBack = feedback, Rating = rating };

            //    //IAmazonSeller amazonSeller = new AmazonSeller() { Details = amazonParser.GetDetail() };
            //});      

            //==================================
            #endregion
            SellerDB sellerDB = new SellerDB();
            Object thisLock = new Object();

            //sellerIDs = new List<string>();
            //sellerIDs.Add("A2W7CFKXL5L9UW");

            //Parallel.ForEach(sellerIDs, new ParallelOptions() { MaxDegreeOfParallelism = 1 }, sellerID =>
            //{
            foreach (var sellerID in sellerIDs)
            {
                SellerEntity sellerEntity = sellerDB.col.FindOne(Query.EQ("details.sellerID", sellerID));

                if (sellerEntity == null)
                    sellerEntity = SetNew();

                lock (thisLock)
                {
                    GetAndSave(sellerDB, sellerID, sellerEntity);
                }

                Console.WriteLine();
            }
            //});
        }

        public Details GetDetail()
        {
            return null;
        }

        private static void GetAndSave(SellerDB sellerDB, string sellerID, SellerEntity sellerEntity)
        {
            sellerEntity.details.sellerID = sellerID;
            sellerEntity.details = new Details(sellerEntity).Get();

            Console.Write(sellerID);
            if (sellerEntity.details != null)
            {
                Console.Write("\tDetails:true");
                sellerEntity.details.feedbackHistory = new FeedBack(sellerEntity).Get();

                sellerEntity.plus = new BsonDocument();
                sellerEntity.plus["lastTimeChecked"] = DateTime.Now.Ticks;

                sellerDB.Save(sellerEntity);
            }

            else
                Console.WriteLine("\tDetails:false");
        }

        private static SellerEntity SetNew()
        {
            SellerEntity sellerEntity;
            sellerEntity = new SellerEntity();
            sellerEntity.id = new ObjectId();
            sellerEntity.details = new DN_Classes.Entities.Seller.Details();
            sellerEntity.details.feedbackHistory = new DN_Classes.Entities.Seller.FeedBack();
            sellerEntity.plus = new BsonDocument();
            return sellerEntity;
        }
    }
}
