﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MongoDB.Bson;
using MongoDB.Driver;

namespace Sellers
{
    public class FlexibleItems
    {
        private static MongoCollection<BsonDocument> col = new MongoClient("mongodb://client148:client148devnetworks@136.243.44.111/RepricerCollection")
                                            .GetServer()
                                            .GetDatabase("RepricerCollection")
                                            .GetCollection<BsonDocument>("flexibleitems");

        /// <summary>
        /// Will get all selleID in RepricerCollection.flexibleitems
        /// </summary>
        /// <returns>List<string></returns>
        public static List<string> GetSellerIDs()
        {
            Console.WriteLine("Getting SellerIDs");
            List<string> sellerIDs = new List<string>() { };

            #region
            //int itemFoundCounter = 0;
            //Parallel.ForEach(col.FindAll().SetFields("sellerList"), new ParallelOptions() { MaxDegreeOfParallelism = 20 }, item =>
            //{
            //    try
            //    {
            //        foreach (var seller in item["sellerList"].AsBsonArray)
            //        {
            //            sellerIDs.Add(seller.ToString().Trim());
            //            Console.Write("\r{0} id(s)", itemFoundCounter);
            //            itemFoundCounter++;
            //        }
            //    }
            //    catch { /*no sellerList element found.*/}
            //});
            #endregion


            int itemFoundCounter = 0;
            foreach (var item in col.FindAll().SetFields("sellerList"))
            {
                try
                {
                    foreach (var seller in item["sellerList"].AsBsonArray)
                    {
                        sellerIDs.Add(seller.ToString().Trim());
                        Console.Write("\r{0} id(s)", itemFoundCounter);

                        itemFoundCounter++;
                    }
                }
                catch { /*no sellerList element found.*/}
            }

            sellerIDs = sellerIDs.Distinct().ToList();
            Console.WriteLine("Done");

            return sellerIDs;
        }

    }
}
