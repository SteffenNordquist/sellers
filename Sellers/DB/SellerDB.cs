﻿using DN_Classes.Entities.Seller;
using MongoDB.Driver;
using MongoDB.Driver.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sellers
{
    public class SellerDB
    {
        public MongoCollection<SellerEntity> col = new MongoClient(@"mongodb://client148:client148devnetworks@136.243.44.111/Sellers")
                                                    .GetServer()
                                                    .GetDatabase("Sellers")
                                                    .GetCollection<SellerEntity>("sellers");
        private readonly IMongoQuery query;

        public SellerDB()
        {
        }

        public void Save(SellerEntity seller)
        {
            col.Save(seller);
        }
    }
}
