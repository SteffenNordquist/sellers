﻿using DN_Classes.Entities.Seller;
using HtmlAgilityPack;
using MongoDB.Bson;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace Sellers
{
    class Program
    {
        static void Main(string[] args)
        {
            DN_Classes.AppStatus.ApplicationStatus s = new DN_Classes.AppStatus.ApplicationStatus("AmazonSellerInfo" + "_148");
            s.AddMessagLine("");
            s.Start();
            try
            {

                Console.WriteLine("AmazonSellersInfo");
                Console.WriteLine("Running...");
                Console.WriteLine();
                Start();
                s.Successful();
                s.Stop();
                s.Dispose();
            }

            catch (Exception ex)
            {
                s.Stop();
                s.AddMessagLine(ex.Message + "\n" + ex.StackTrace);
                s.Dispose();
            }
        }

        private static void Start()
        {
            AmazonParser amazonParser = new AmazonParser(FlexibleItems.GetSellerIDs());
            amazonParser.GetSellerInfo();

        }
    }
}
